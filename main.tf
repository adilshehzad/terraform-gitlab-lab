terraform {
  required_providers {
    aws={
        source="hashicorp/aws"
        version = "4.57.0"
    }
  }
}

provider "aws" {
    region = var.region
  
}

# Creating VPC
resource "aws_vpc" "my_vpc" {
    cidr_block = var.my_cidr_id
    tags = {
      "Name" = "my_vpc"
    }
  
}

# Creating Subnet 1

resource "aws_subnet" "my_subnet_1" {

    vpc_id = aws_vpc.my_vpc.id
    cidr_block = "10.0.1.0/24"
    availability_zone = var.availability_zones[0]
  
}

# Creating Subnet 2

resource "aws_subnet" "my_subnet_2" {
    
        vpc_id = aws_vpc.my_vpc.id
        cidr_block = "10.0.2.0/24"
        availability_zone = var.availability_zones[1]
  
}

# Creating Internet Gateway 

resource "aws_internet_gateway" "my_internet_gateway" {
    vpc_id = aws_vpc.my_vpc.id
    tags = {
      "Name" = "my_internet_gateway"
    }
  
}
  


# Creating Route Tables

resource "aws_route_table" "my_route_table" {
    vpc_id = aws_vpc.my_vpc.id
    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.my_internet_gateway.id
    }
    tags = {
      "Name" = "my_route_table"
    }
}

# Creating Route Table Association

resource "aws_route_table_association" "rta1" {
    subnet_id = aws_subnet.my_subnet_1.id
    route_table_id = aws_route_table.my_route_table.id
  
}

# Creating Route Table Association 2 

resource "aws_route_table_association" "rta2" {
    subnet_id = aws_subnet.my_subnet_2.id
    route_table_id = aws_route_table.my_route_table.id
  
}

# Creating Security Group
resource "aws_security_group" "my_security_group" {
    name = "my_security_group"
    description = "Allow SSH and HTTP traffic"
    vpc_id = aws_vpc.my_vpc.id
    dynamic "ingress"{

        for_each = var.ingress_cidr_blocks
        content {
            from_port = ingress.value.from_port
            to_port = ingress.value.to_port
            protocol = ingress.value.protocol
            cidr_blocks = ingress.value.cidr_blocks
        }
    }
    dynamic "egress"{
        for_each = var.engress_cidr_blocks
        content {
            from_port = egress.value.from_port
            to_port = egress.value.to_port
            protocol = egress.value.protocol
            cidr_blocks = egress.value.cidr_blocks
        }
    }
    tags = {
      "Name" = "Terraform_Security_Group"
    }
}

# Creating Data Source for AMI

data "aws_ami" "ubuntu" {
    most_recent = true
    owners = ["amazon"]
    filter {
      name = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
    }
}

# Creating EC2 Instance

resource "aws_instance" "web-server" {
  ami                          = data.aws_ami.ubuntu.id
  instance_type                = var.instance_type
  key_name                     = var.my_key
  vpc_security_group_ids       = [aws_security_group.my_security_group.id]
  subnet_id                    = aws_subnet.my_subnet_1.id
  associate_public_ip_address  = true
  user_data = <<-EOT
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install -y nginx
    sudo systemctl start nginx
    sudo systemctl enable nginx
    sudo ufw allow 'Nginx HTTP'
    sudo ufw status
    EOT

  tags = {
    "Name" = "web-server"
  }
}
