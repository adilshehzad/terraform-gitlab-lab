output "my_vpc" {
    value = aws_vpc.my_vpc.id
  
}

output "my_subnet_1" {
    value = aws_subnet.my_subnet_1.id
  
}

output "my_subnet_2" {
    value = aws_subnet.my_subnet_2.id
  
}

output "ec2_instance_ip"{
    value=aws_instance.web-server.public_ip
}