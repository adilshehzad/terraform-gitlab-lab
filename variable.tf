variable "region" {
    description = "The region to deploy to"
    default = "us-east-1"
  
}


variable "my_cidr_id" {
    description = "The CIDR block to use"
    type = string
    default = "10.0.0.0/16"


}

variable "availability_zones" {
    description = "The availability zones to use"
    type = list(string)
    default = ["us-east-1a", "us-east-1b"]
  
  
}

variable "ingress_cidr_blocks" {
    description = "The ingress CIDR blocks to use"
    type = list(object({
      from_port = number
      to_port = number
      protocol = string
      cidr_blocks = list(string)
    }))

    default = [
      {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"] # Its better to use your local system IP address
      },
    
      {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
        }
    ]

}

variable "engress_cidr_blocks" {
    description = "The egress CIDR blocks to use"
    type = list(object({
      from_port = number
      to_port = number
      protocol = string
      cidr_blocks = list(string)
    }))

    default = [
      {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
      }
    ]
  
}

variable "instance_type" {
    description = "The instance type to use"
    default = "t2.medium"
  
  
}
      

variable "my_key" {
    description = "The key to use"
    default = "adil-tf"
  
  
}
