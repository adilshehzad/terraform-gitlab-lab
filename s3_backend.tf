terraform {
  backend "s3" {
    bucket = "myterraformstatebucket-adil"
    key    = "my-lab/terraform.tfstate"
    region = "us-east-1"
  }
}
